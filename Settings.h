#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <string>

#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSettings : public CObject  
{
	DECLARE_SERIAL(CSettings);

public:
	CSettings();
	virtual ~CSettings();

public:
	BOOL Load(LPCTSTR lpszProfileName);
	BOOL Save(LPCTSTR lpszProfileName);
	void MakeStatusMsg();
	void UpdateStatusMsg(unsigned char);
public:
	DWORD m_dwPollingPeriod;
	BOOL m_bTestLoopback;
	BOOL m_bShowSIOMessages;
	BOOL m_bShowMessageErrors;
	BOOL m_bShowCOMErrors;
	std::string m_strSettingsReportPath;

	UINT m_nBufferSize;

	std::string m_strIncomingAddress;
	UINT m_nIncomingPort;

	std::string m_strCOMSetup;
	int m_iCOMRttc;
	int m_iCOMWttc;
	int m_iCOMRit;

	std::vector<BYTE> m_arPrefix;
	std::vector<BYTE> m_arOutPrefix;

	WORD m_wComposedType;
	WORD m_wOutputComposedType;
	WORD m_wCRC16Init;
	WORD m_wCPAddr;
	WORD m_wPUAddr;
	typedef struct tagMESSAGETYPE
	{
		WORD m_wType;			//���
		WORD m_wMaxLength;		//max ����� ��������� ��� ����
		WORD m_wDestination;	//����������
		WORD m_wSource;			//�����������
		WORD m_wDestMask;		//����� ���������� (������� ����������)
		WORD m_wSrcMask;		//����� ����������� (������� ����������)
		tagMESSAGETYPE(WORD wType = 0, WORD wMaxLength = 0, WORD wDestination = 0, WORD wSource = 0, WORD wDestMask = 0, WORD wSrcMask = 0)
		{
			m_wType = wType;
			m_wMaxLength = wMaxLength;
			m_wDestination = wDestination;
			m_wSource = wSource;
			m_wDestMask = wDestMask;
			m_wSrcMask = wSrcMask;
		}
	} MESSAGETYPE, * PMESSAGETYPE;
	std::map<WORD, MESSAGETYPE> m_mapMsgTypes;
	std::map<WORD, void*> m_mapMsgTypesToUnpack;
	BOOL m_bUnpackAll;
	std::map<WORD, void*> m_mapMsgTypesToMark;
	BOOL m_bMarkAll;

	UINT m_nStatusPeriod;
	int m_iSendStatTO;
	MESSAGETYPE m_StatusHdr;
	MESSAGETYPE m_StatusMsg;
	MESSAGETYPE m_MarkNestedMask;
	MESSAGETYPE m_MarkComposedMask;
	WORD m_TUType;
	WORD m_TUSrcMask;
	BOOL m_TUSrcComMsgIndex;
	UINT m_TUPrimToSecSrc;
	UINT m_TUSecToPrimSrc;

	std::vector<BYTE> m_arStatusData;
	std::vector<BYTE> m_arStatusMsg;

	BOOL m_bKeepLog;
	WORD m_wLogComposedType;
	std::map<WORD, void*> m_mapLogMsgTypesToUnpack;
	BOOL m_bLogUnpackAll;

	WORD m_wLogComposedTypeToPack;
	std::map<WORD, void*> m_mapLogMsgTypesToPack;
	BOOL m_bLogPackAll;
	
	//������ ugs
	WORD m_wSourceID;
	WORD m_wStatusRequestMessageType;
};

void replDef(unsigned short& expl, const unsigned short& def);
inline void ltrim(std::string& s);
inline void rtrim(std::string& s);
inline std::string& trim(std::string& s);
inline std::string& tolower(std::string& s);
inline std::string tokenize(const std::string& str, const std::string& tokens, int& start);

extern CSettings g_Settings;

#endif // _SETTINGS_H_
