#include "stdafx.h"
#include "gtest/gtest.h"

using namespace std;

TEST(TestSettings, TestTrim) {
	string str1 = "   hello       ";
	string str2 = "hello";
	string str3 = "hello   \n\n";
	ASSERT_EQ(trim(str1), "hello");
	ASSERT_EQ(trim(str2), "hello");
	ASSERT_EQ(trim(str3), "hello");
}

TEST(TestSettings, TestToLower) {
	string str = "HeLlO, wOrLd!";
	ASSERT_EQ(tolower(str), "hello, world!");
}

TEST(TestSettings, TestTokenize) {
	string str = "  one   two    three ";
	int pos = 0;
	vector<string> tokens;
	for (int i = 0; i < 3; ++i)
		tokens.push_back(tokenize(str, " ", pos));
	ASSERT_EQ(tokens[0], "one");
	ASSERT_EQ(tokens[1], "two");
	ASSERT_EQ(tokens[2], "three");
}