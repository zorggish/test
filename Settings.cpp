#include "stdafx.h"
#include "Settings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSettings g_Settings;

IMPLEMENT_SERIAL(CSettings, CObject, 1)

CSettings::CSettings()
{
	m_dwPollingPeriod = 1000;
	m_bTestLoopback = FALSE;
	m_bShowSIOMessages = FALSE;
	m_bShowMessageErrors = FALSE;
	m_bShowCOMErrors = FALSE;
	m_strSettingsReportPath = _T("ugs.rep");

	m_nBufferSize = 0x90000;

	m_nIncomingPort = 11112;

	m_strCOMSetup = _T("COM1: baud=9600 data=8 parity=N stop=1");
	m_iCOMRttc = 0;
	m_iCOMWttc = 0;
	m_iCOMRit = -1;

	m_arPrefix.clear();

	m_wComposedType = 0x000003;
	m_wOutputComposedType = 0x0000;
	m_wCRC16Init = 0xFFFF;

	m_wCPAddr = 0x0000;
	m_wPUAddr = 0x0000;

	m_bUnpackAll = FALSE;
	m_bMarkAll = FALSE;

	m_nStatusPeriod = 0;
	m_iSendStatTO = 1000000;
	m_StatusHdr = MESSAGETYPE(0x0000, 0x20);
	m_StatusMsg = MESSAGETYPE(m_wComposedType);
	m_MarkNestedMask = MESSAGETYPE();
	m_MarkComposedMask = MESSAGETYPE();
	m_arStatusData.clear();
	MakeStatusMsg();
	UpdateStatusMsg(0);
	m_TUType = 0x000002;
	m_TUSrcMask = 0x0000;
	m_TUSrcComMsgIndex = FALSE;
	m_TUPrimToSecSrc = 1;
	m_TUSecToPrimSrc = 1;


	m_bKeepLog = FALSE;
	m_wLogComposedType = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wLogComposedTypeToPack = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wSourceID = 0x000020;
	m_wStatusRequestMessageType = 0x0001;

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes[0x0001] = typeStatus;
}

CSettings::~CSettings()
{
}

void replDef(unsigned short& expl, const unsigned short& def)
{
  expl = expl ? expl : def;
}

inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) { return !std::isspace(ch); }));
}

inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) { return !std::isspace(ch); }).base(), s.end());
}

inline std::string& trim(std::string& s) {
	ltrim(s);
	rtrim(s);
	return s;
}

inline std::string& tolower(std::string& s) {
	std::transform(s.begin(), s.end(), s.begin(), [](char c) { return std::tolower(c); });
	return s;
}

inline std::string tokenize(const std::string &str, const std::string &tokens, int& start)
{
	std::string result;
	while (tokens.find(str[start]) != tokens.npos) ++start;
	while (tokens.find(str[start]) == tokens.npos) result += str[start++];
	return result;
}

BOOL CSettings::Load(LPCTSTR lpszProfileName)
{
	try
	{
		std::ifstream file(std::string(lpszProfileName), std::ios_base::in);

		std::map<std::string, std::string> mapSettings;
		std::string strGroup(_T("[General]"));

		std::string strLine;
		while(std::getline(file, strLine))
		{
			trim(strLine);
			int iComment = strLine.find(_T(';'), 0);
			if(iComment >= 0)
				strLine.erase(iComment, strLine.length() - iComment);

			if(strLine.empty())
				continue;

			if(strLine.find(_T('[')) == 0 && strLine.rfind(_T(']')) == strLine.length() - 1)
			{
				strGroup = strLine;
				continue;
			}

			int iPos = 0;
			
			std::string tokenized = tokenize(strLine, _T("="), iPos);
			std::string strKey(strGroup + std::string(_T("/")) + trim(tokenized));
			std::string strValue(strLine.begin() + iPos, strLine.end());

			//mapSettings[strKey] = trim(strValue).Trim(_T("\""));
			mapSettings[strKey] = trim(strValue);
		}

		int iValue;

		auto result = mapSettings.find(_T("[General]/PollingPeriod"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
			m_dwPollingPeriod = (DWORD)iValue;

		result = mapSettings.find(_T("[General]/TestLoopback"));
		if(result != mapSettings.end())
			m_bTestLoopback = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[General]/ShowSIOMessages"));
		if (result != mapSettings.end())
			m_bShowSIOMessages = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[General]/ShowMessageErrors"));
		if (result != mapSettings.end())
			m_bShowMessageErrors = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[General]/ShowCOMErrors"));
		if (result != mapSettings.end())
			m_bShowCOMErrors = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[General]/SettingsReportPath"));
		if (result != mapSettings.end())
			m_strSettingsReportPath = result->second;

		result = mapSettings.find(_T("[General]/BufferSize"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue) && iValue != 0)
			m_nBufferSize = (UINT)iValue;

		result = mapSettings.find(_T("[General]/IncomingPort"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
			m_nIncomingPort = (UINT)iValue;

		result = mapSettings.find(_T("[UDP]/OutgoingIP"));
		if(result != mapSettings.end())
		{
			DWORD dwTemp = (DWORD)inet_addr(result->second.c_str());
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(result->second.c_str());
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}

		}

		
		result = mapSettings.find(_T("[COM]/SetupParams"));
		if (result != mapSettings.end())
			m_strCOMSetup = result->second;

		result = mapSettings.find(_T("[COM]/rttc"));
		if (result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMRttc = iValue;

		result = mapSettings.find(_T("[COM]/wttc"));
		if (result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMWttc = iValue;

		result = mapSettings.find(_T("[COM]/rit"));
		if (result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMRit = iValue;

		std::vector<BYTE> arTemp;
		result = mapSettings.find(_T("[Message]/CPAddr"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wCPAddr = (WORD)iValue;
		auto puaddr = mapSettings.find(_T("[Message]/PUAddr"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wPUAddr = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/Prefix"));
		if(result != mapSettings.end() && ByteArrayFromString(result->second, arTemp, _T("")))
			m_arPrefix = arTemp;

		result = mapSettings.find(_T("[Message]/OutPrefix"));
		if(result != mapSettings.end() && ByteArrayFromString(result->second, arTemp, _T("")))
			m_arOutPrefix = arTemp;

		result = mapSettings.find(_T("[Message]/CRC16Init"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wCRC16Init = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/ComposedType"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wComposedType = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/OutputComposedType"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wOutputComposedType = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/TypesToUnPack"));
		if(result != mapSettings.end())
		{
			m_mapMsgTypesToUnpack.clear();
			if(result->second == _T("*"))
			{
				m_mapMsgTypesToUnpack[0x0000] = NULL;
				m_bUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < result->second.length() - 1; )
				{
					if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToUnpack[(WORD)iValue] = NULL;
				}
				m_bUnpackAll = FALSE;
			}
		}

		result = mapSettings.find(_T("[Message]/MarkComposedMessageMask"));
		if(result != mapSettings.end())
		{
			int iPos = 0;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wSrcMask = (WORD)iValue;
		}

		result = mapSettings.find(_T("[Message]/MarkMessageMask"));
		if(result != mapSettings.end())
		{
			int iPos = 0;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wSrcMask = (WORD)iValue;
		}
		
		result = mapSettings.find(_T("[Message]/TypesToMark"));
		if(result != mapSettings.end())
		{
			m_mapMsgTypesToMark.clear();
			if(result->second == _T("*"))
			{
				m_mapMsgTypesToMark[0x0000] = NULL;
				m_bMarkAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < result->second.length() - 1; )
				{
					if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToMark[(WORD)iValue] = NULL;
				}
				m_bMarkAll = FALSE;
			}
		}

		m_mapMsgTypes.clear();

		for(int i = 1; i < 10; i++)
		{
			char buffer[32];
			snprintf(buffer, 32, _T("[Message]/Type%u"), i);
			std::string strTemp(buffer);

			result = mapSettings.find(strTemp);
			if (result == mapSettings.end())
				continue;

			MESSAGETYPE type;
			int iPos = 0;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wType = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wMaxLength = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wSource = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wSrcMask = (WORD)iValue;

			if(type.m_wType == 0x0505) {
				replDef(type.m_wSource, m_wPUAddr);
				replDef(type.m_wDestination, m_wCPAddr);
			}
			else if(type.m_wType == 0x0521 || type.m_wType == 0x0532) {
				replDef(type.m_wSource, m_wCPAddr);
			}
			else {
				replDef(type.m_wSource, m_wCPAddr);
				replDef(type.m_wDestination, m_wPUAddr);
			}
			
			m_mapMsgTypes[type.m_wType] = type;            
		}

		MESSAGETYPE typeStatus(0x0001, 0x1000);
		m_mapMsgTypes[0x0001] = typeStatus;

		result = mapSettings.find(_T("[Message]/StatusPeriod"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_nStatusPeriod = (UINT)iValue;

		result = mapSettings.find(_T("[Message]/SendStatusTO"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_iSendStatTO = (int)iValue;

		result = mapSettings.find(_T("[Message]/StatusMsg"));
		if(result != mapSettings.end())
		{
			int iPos = 0;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wType = (WORD)iValue;
			if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wDestination = (WORD)iValue;
			if (::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wSource = (WORD)iValue;
			if (::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wType = (WORD)iValue;
			if (::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wDestination = (WORD)iValue;
			if (::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wSource = (WORD)iValue;
			ByteArrayFromString(tokenize(result->second, _T(" "), iPos).c_str(), arTemp, _T(""));
			m_arStatusData = arTemp;

			replDef(m_StatusMsg.m_wSource, m_wCPAddr);
			replDef(m_StatusMsg.m_wDestination, m_wPUAddr);

			MakeStatusMsg();
			UpdateStatusMsg(0);
		}

		result = mapSettings.find(_T("[Message]/TUType"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_TUType = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/TUSrcMask"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_TUSrcMask = (WORD)iValue;

		result = mapSettings.find(_T("[Message]/TUSrcComMsgIndex"));
		if(result != mapSettings.end())
			m_TUSrcComMsgIndex = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[Message]/TUPrimToSecSrc"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_TUPrimToSecSrc = (UINT)iValue;

		result = mapSettings.find(_T("[Message]/TUSecToPrimSrc"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_DEFAULT, &iValue))
			m_TUSecToPrimSrc = (UINT)iValue;

		result = mapSettings.find(_T("[Message]/KeepLog"));
		if(result != mapSettings.end())
			m_bKeepLog = tolower(result->second) == _T("1") ? TRUE : FALSE;

		result = mapSettings.find(_T("[Message]/LogIP"));
		if(result != mapSettings.end())
		{
			DWORD dwTemp = (DWORD)inet_addr(result->second.c_str());
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(result->second.c_str());
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}
		}

		result = mapSettings.find(_T("[Log]/LogComposedType"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedType = (WORD)iValue;

		result = mapSettings.find(_T("[Log]/LogTypesToUnPack"));
		if(result != mapSettings.end())
		{
			m_mapLogMsgTypesToUnpack.clear();
			if(result->second == _T("*"))
			{
				m_mapLogMsgTypesToUnpack[0x0000] = NULL;
				m_bLogUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < result->second.length() - 1; )
				{
					if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToUnpack[(WORD)iValue] = NULL;
				}
				m_bLogUnpackAll = FALSE;
			}
		}

		result = mapSettings.find(_T("[Log]/LogComposedTypeToPack"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedTypeToPack = (WORD)iValue;

		result = mapSettings.find(_T("[Log]/LogTypesToPack"));
		if(result != mapSettings.end())
		{
			m_mapLogMsgTypesToPack.clear();
			if(result->second == _T("*"))
			{
				m_mapLogMsgTypesToPack[0x0000] = NULL;
				m_bLogPackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < result->second.length() - 1; )
				{
					if(::StrToIntEx(tokenize(result->second, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToPack[(WORD)iValue] = NULL;
				}
				m_bLogPackAll = FALSE;
			}
		}

		result = mapSettings.find(_T("[Status]/SourceIndex"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wSourceID = (WORD)iValue;

		result = mapSettings.find(_T("[Status]/StatusRequestMessageType"));
		if(result != mapSettings.end() && ::StrToIntEx(result->second.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wStatusRequestMessageType = (WORD)iValue;

		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

BOOL CSettings::Save(LPCTSTR lpszProfileName)
{
	try
	{
		CStdioFile file(lpszProfileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::typeText);
		TCHAR buffer[1024];

		file.WriteString(_T("[General]\n"));
		sprintf(buffer, _T("PollingPeriod = %u\n"), m_dwPollingPeriod);
		file.WriteString(buffer);
		sprintf(buffer, _T("TestLoopback = %s\n"), m_bTestLoopback ? _T("1") : _T("0"));
		file.WriteString(buffer);
		sprintf(buffer, m_bShowSIOMessages ? _T("1") : _T("0"));
		file.WriteString(buffer);
		sprintf(buffer, _T("ShowMessageErrors = %s\n"), m_bShowMessageErrors ? _T("1") : _T("0"));
		file.WriteString(buffer);
		sprintf(buffer, _T("ShowCOMErrors = %s\n"), m_bShowCOMErrors ? _T("1") : _T("0"));
		file.WriteString(buffer);
		sprintf(buffer, _T("SettingsReportPath = \"%s\"\n"), m_strSettingsReportPath.c_str());
		file.WriteString(buffer);
		sprintf(buffer, _T("BufferSize = 0x%04X\n"), m_nBufferSize);
		file.WriteString(buffer);
		file.WriteString(_T("[UDP]\n"));
		sprintf(buffer, _T("IncomingPort = %u\n"), m_nIncomingPort);
		file.WriteString(buffer);
		file.WriteString(_T("[COM]\n"));
		sprintf(buffer, _T("SetupParams = \"%s\"\n"), m_strCOMSetup.c_str());
		file.WriteString(buffer);
		sprintf(buffer, _T("rttc = %i\n"), m_iCOMRttc);
		file.WriteString(buffer);
		sprintf(buffer, _T("wttc = %i\n"), m_iCOMWttc);
		file.WriteString(buffer);
		sprintf(buffer, _T("rit = %i\n"), m_iCOMRit);
		file.WriteString(buffer);
		file.WriteString(_T("[Message]\n"));
		sprintf(buffer, _T("CPAddr = 0x%04X\n"), m_wCPAddr);
		file.WriteString(buffer);
		sprintf(buffer, _T("PUAddr = 0x%04X\n"), m_wPUAddr);
		file.WriteString(buffer);

		std::string strTemp;
		ByteArrayToString(m_arPrefix.data(), (int)m_arPrefix.size(), strTemp, _T("Prefix = \""));
		file.WriteString(strTemp.c_str());
		file.WriteString(_T("\"\n"));
		sprintf(buffer, _T("CRC16Init = 0x%04X\n"), m_wCRC16Init);
		file.WriteString(buffer);
		sprintf(buffer, _T("ComposedType = 0x%04X\n"), m_wComposedType);
		file.WriteString(buffer);
		sprintf(buffer, _T("OutputComposedType = 0x%04X\n"), m_wOutputComposedType);
		file.WriteString(buffer);
		file.WriteString(_T("TypesToUnPack = \""));
		if(m_bUnpackAll)
			file.WriteString(_T("*"));
		else
		{
			for (auto pos = m_mapMsgTypesToUnpack.begin(); pos != m_mapMsgTypesToUnpack.end(); ++pos)
			{
				WORD wType = pos->first;
				void* pTemp = pos->second;
				sprintf(buffer, _T("%04X "), wType);
				file.WriteString(buffer);
			}
		}
		file.WriteString(_T("\"\n"));

		sprintf(buffer, _T("MarkComposedMessageMask  = \"0x%04X 0x%04X\"\n"), m_MarkComposedMask.m_wDestMask,
			m_MarkComposedMask.m_wSrcMask);
		file.WriteString(buffer);
		sprintf(buffer, _T("MarkMessageMask  = \"0x%04X 0x%04X\"\n"), m_MarkNestedMask.m_wDestMask,
			m_MarkNestedMask.m_wSrcMask);
		file.WriteString(buffer);
		file.WriteString(_T("TypesToMark = \""));
		if(m_bMarkAll)
			file.WriteString(_T("*"));
		else
		{
			for (auto pos = m_mapMsgTypesToMark.begin(); pos != m_mapMsgTypesToMark.end(); ++pos)
			{
				WORD wType = pos->first;
				void* pTemp = pos->second;
				sprintf(buffer, _T("%04X "), wType);
				file.WriteString(buffer);
			}
		}
		file.WriteString(_T("\"\n"));

		auto pos = m_mapMsgTypes.begin();
		for(int i = 0; pos != m_mapMsgTypes.end() && i < 10; i++)
		{
			WORD wType = pos->first;
			MESSAGETYPE type = pos->second;
			++pos;
			sprintf(buffer, _T("Type%u = \"0x%04X 0x%X 0x%04X 0x%04X 0x%04X 0x%04X\"\n"), i, type.m_wType,
				type.m_wMaxLength, type.m_wDestination, type.m_wSource, type.m_wDestMask, type.m_wSrcMask);
			file.WriteString(buffer);
		}

		sprintf(buffer, _T("StatusPeriod = %u\n"), m_nStatusPeriod);
		file.WriteString(buffer);
		sprintf(buffer, _T("SendStatusTO = %u\n"), m_iSendStatTO);
		file.WriteString(buffer);
		sprintf(buffer, _T("StatusMsg = \"0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X "), m_StatusHdr.m_wType,
			m_StatusHdr.m_wDestination, m_StatusHdr.m_wSource, m_StatusMsg.m_wType, m_StatusMsg.m_wDestination,
			m_StatusMsg.m_wSource);
		file.WriteString(buffer);
		ByteArrayToString(m_arStatusData.data(), m_arStatusData.size(), strTemp);
		file.WriteString(strTemp.c_str());
		file.WriteString(_T("\"\n"));

		sprintf(buffer, _T("TUType = 0x%04X\n"), m_TUType);
		file.WriteString(buffer);

		sprintf(buffer, _T("TUSrcMask = 0x%04X\n"), m_TUSrcMask);
		file.WriteString(buffer);
		sprintf(buffer, _T("TUSrcComMsgIndex = %s\n"), m_TUSrcComMsgIndex ? _T("1") : _T("0"));
		file.WriteString(buffer);

		sprintf(buffer, _T("TUPrimToSecSrc = %u\n"), m_TUPrimToSecSrc);
		file.WriteString(buffer);
		sprintf(buffer, _T("TUSecToPrimSrc = %u\n"), m_TUSecToPrimSrc);
		file.WriteString(buffer);
		ByteArrayToString(m_arOutPrefix.data(), m_arOutPrefix.size(), strTemp, _T("OutPrefix = \""));
		file.WriteString(strTemp.c_str());
		file.WriteString(_T("\"\n"));

		file.WriteString(_T("[Log]\n"));
		sprintf(buffer, _T("KeepLog = %s\n"), m_bKeepLog ? _T("1") : _T("0"));
		file.WriteString(buffer);
		sprintf(buffer, _T("LogComposedType = 0x%04X\n"), m_wLogComposedType);
		file.WriteString(buffer);
		file.WriteString(_T("LogTypesToUnPack = \""));
		if(m_bLogUnpackAll)
			file.WriteString(_T("*"));
		else
		{
			for (auto pos = m_mapMsgTypesToUnpack.begin(); pos != m_mapMsgTypesToUnpack.end(); ++pos)
			{
				WORD wType = pos->first;
				void* pTemp = pos->second;
				sprintf(buffer, _T("%04X "), wType);
				file.WriteString(buffer);
			}
		}
		file.WriteString(_T("\"\n"));


		sprintf(buffer, _T("LogComposedTypeToPack = 0x%04X\n"), m_wLogComposedTypeToPack);
		file.WriteString(buffer);
		file.WriteString(_T("LogTypesToPack = \""));
		if(m_bLogPackAll)
			file.WriteString(_T("*"));
		else
		{
			for (auto pos = m_mapLogMsgTypesToPack.begin(); pos != m_mapLogMsgTypesToPack.end(); ++pos)
			{
				WORD wType = pos->first;
				void* pTemp = pos->second;
				sprintf(buffer, _T("0x%04X "), wType);
				file.WriteString(buffer);
			}
		}
		file.WriteString(_T("\"\n"));


		file.WriteString(_T("[Status]\n"));

		sprintf(buffer, _T("SourceIndex = 0x%04X\n"), m_wSourceID);
		file.WriteString(buffer);
		sprintf(buffer, _T("StatusRequestMessageType = 0x%04X\n"), m_wStatusRequestMessageType);
		file.WriteString(buffer);

		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

void CSettings::MakeStatusMsg()
{
	m_StatusMsg.m_wMaxLength = 16 + (WORD)m_arStatusData.size();
	m_StatusHdr.m_wMaxLength = 16 + m_StatusMsg.m_wMaxLength;
	m_arStatusMsg.resize(m_StatusHdr.m_wMaxLength);
	BYTE * pData = m_arStatusMsg.data();
	::ZeroMemory(pData, m_StatusHdr.m_wMaxLength);
	WORD * pHeader = (WORD *)pData;
	pHeader[0] = m_StatusHdr.m_wMaxLength;
	pHeader[1] = m_StatusHdr.m_wType;
	pHeader[2] = m_StatusHdr.m_wDestination;
	pHeader[3] = m_StatusHdr.m_wSource;
	pHeader[7] = m_StatusMsg.m_wMaxLength;
	pHeader[8] = m_StatusMsg.m_wType;
	pHeader[9] = m_StatusMsg.m_wDestination;
	pHeader[10] = m_StatusMsg.m_wSource;
	memcpy(pData + 28, m_arStatusData.data(), m_arStatusData.size());
}

void CSettings::UpdateStatusMsg(unsigned char ind)
{
	BYTE * pData = m_arStatusMsg.data();
	UINT nLength = (UINT)m_arStatusMsg.size();
	*((DWORD *)(pData + 8)) = *((DWORD *)(pData + 22)) = (DWORD)time(NULL);
	pData[12] = ind;
	pData[26] = ind;
	*((WORD *)(pData + nLength - sizeof(DWORD))) = CRC16(pData + 14, nLength - 14 - sizeof(DWORD), m_wCRC16Init);
	*((WORD *)(pData + nLength - sizeof(WORD))) = CRC16(pData, nLength - sizeof(WORD), m_wCRC16Init);
}
